# RPi Kernel Module CMake

Based on a hello world kernel module cmake configuration by Christoph Acham.

It has been modified to build on the raspberry pi as well as expanded to make it easier to drop in your own project.


## Setting up the project

The source project will simply need to be structured in a specific way to automatically work with the CMake files.

The following directory structure shows how the project named 'hello' will need to be structured.

```
|--hello
 \
  |
  `--build
  |
  `--include
  |
  `--lib
  |
  `--src
  |
  +--hello_driver.c
```

**hello**
> the directory containing the project files and CMake files

**hello/build**
>  the directory CMake will generate build files in

**hello/include**
>  the directory for your project header files and any 3rd-party library headers

**hello/lib**
>  the directory for your project specific libraries

**hello/src**
>  the directory for your project source files 

**hello/hello_driver.c**
>  the main driver source file. The naming of this file will need to match the project name. There is an example_driver.c included for refactoring if needed.


## Native Compile

The easiest way to obtain everything needed is to download the \"*Raspberry Pi OS with desktop and recommended software*\" from [Raspberrypi.org](https://www.raspberrypi.org/software/operating-systems/) and setup an SD card according to those directions. You will need to manually enable ssh and whichever other protocols you expect to use.

1) Copy the contents of a template folder into your project directory, after it has been properly created according to the steps above.

2) Open a terminal window
3) Navigate into the build directory of the project ``` cd /path/to/project/ ```
4) run cmake: ``` cmake ../ ```
5) build the project: ``` make ```

## Cross-compile

#### From Windows
Cross-compiling from Windows is not directly supported. You will need to run a linux distro in a virtual environment.

#### From Linux
Before we are able to compile anything for our target version of the raspberry pi, we will need to set up a toolchain and have a copy of the important parts of the root file system from the distribution running on the pi. 

##### Setting up the Raspberry Pi
For the root file system, the easiest way to obtain this is to download the \"*Raspberry Pi OS with desktop and recommended software*\" from [Raspberrypi.org](https://www.raspberrypi.org/software/operating-systems/) and setup an SD card according to those directions. You will need to manually enable ssh and whichever other protocols you expect to use.

To follow these steps on the lite version, or on a non-raspbian distribution, you will need to install and setup the following software and it's dependencies

- gcc (and g++ if seperate)
- libc
- ssh
- rsync
- *kernel api headers*

The kernel headers we are looking for are the *kernel api headers* for your kernel version. The easiest package to find in most repositories are the user-sanitized header files. These are only appropriate for applications, not building kernel modules. The appropriate package generally stores this in a directory under /usr/src/ (Raspbian) or /usr/lib/modules/build (Manjaro)

To make sure you have the base requirements, you can follow the build steps for the included 'hello' module as a test, natively on the pi. Resolve any issues before continuing.

##### Setting up the host toolchain

###### rootfs
We will need to mirror the directories used for compiling and linking C/C++ from the raspberry pi.

Open a new terminal window and run the following steps:
*Note the '.' (dot) in the local path name*
```
mkdir -p ~/.toolchain/rpi/rootfs/usr/include ~/.toolchain/rpi/rootfs/usr/lib ~/.toolchain/rpi/rootfs/usr/src
```
You will need to replace the '192.168.10.10' address with the IP address of your Pi. If the username is different than 'pi' you will need to also change this accordingly.
```
rsync -rchavzP --stats pi@192.168.10.10:/usr/include ~/.toolchain/rpi/rootfs/usr/
rsync -rchavzP --stats pi@192.168.10.10:pi@192.168.10.10:/usr/lib ~/.toolchain/rpi/rootfs/usr
rsync -rchavzP --stats pi@192.168.10.10:/usr/src ~/.toolchain/rpi/rootfs/usr
```

The CMake files assume that the rootfs has been stored in ~/.toolchain/rpi/rootfs. 
This behavior can be overridden by passing the following flag to CMake
- -DTOOLCHAIN_ROOTFS=/path/to/rootfs

###### arm-linux-gnueabihf



