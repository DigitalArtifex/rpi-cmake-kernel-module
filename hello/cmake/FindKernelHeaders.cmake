# Find the kernel release
set(KERNELHEADERS_FOUND 0 CACHE STRING "Set to 1 if kernel headers were found")

if(NOT TOOLCHAIN_CROSS_COMPILE)
    execute_process(
            COMMAND uname -r
            OUTPUT_VARIABLE KERNEL_RELEASE
            OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    execute_process(
        COMMAND lsb_release -i
        OUTPUT_VARIABLE SYSTEM_OS
        OUTPUT_STRIP_TRAILING_WHITESPACE
    )

    if(SYSTEM_OS MATCHES "^Raspbian")
        message(STATUS "Native Build Detected")
        set(KERNEL_PATH_HINT /usr/src/linux-headers-${KERNEL_RELEASE})
    endif()

    find_path(
        KERNELHEADERS_DIR
        include/linux/user.h
        PATHS ${KERNEL_PATH}
    )

    message(STATUS "Kernel release: ${KERNEL_RELEASE}")
    message(STATUS "Kernel headers: ${KERNELHEADERS_DIR}")

    if (KERNELHEADERS_DIR)
        set(
            KERNELHEADERS_INCLUDE_DIRS
            ${KERNELHEADERS_DIR}/include
            ${KERNELHEADERS_DIR}/arch/arm/include
            CACHE PATH "Kernel headers include dirs"
        )
        set(KERNELHEADERS_FOUND 1 CACHE STRING "Set to 1 if kernel headers were found")
    else (KERNELHEADERS_DIR)
        set(KERNELHEADERS_FOUND 0 CACHE STRING "Set to 1 if kernel headers were found")
    endif (KERNELHEADERS_DIR)
else()
    if(EXISTS ${TOOLCHAIN_ROOTFS})
        message(STATUS "Searching for kernel headers in rootfs ${TOOLCHAIN_ROOTFS}")
        set(KERNEL_PATH ${TOOLCHAIN_ROOTFS}/usr/src/linux-headers-${KERNEL_RELEASE})
        find_path(
            KERNELHEADERS_DIR
            include/linux/user.h
            PATHS ${KERNEL_PATH}
        )

        message(STATUS "Kernel release: ${KERNEL_RELEASE}")
        message(STATUS "Kernel headers: ${KERNELHEADERS_DIR}")

        if (KERNELHEADERS_DIR)
            set(
                KERNELHEADERS_INCLUDE_DIRS
                ${KERNELHEADERS_DIR}/include
                ${KERNELHEADERS_DIR}/arch/arm/include
                CACHE PATH "Kernel headers include dirs"
            )
            set(KERNELHEADERS_FOUND 1 CACHE STRING "Set to 1 if kernel headers were found")
        else (KERNELHEADERS_DIR)
            set(KERNELHEADERS_FOUND 0 CACHE STRING "Set to 1 if kernel headers were found")
        endif (KERNELHEADERS_DIR)
    endif()
endif()

mark_as_advanced(KERNELHEADERS_FOUND)
