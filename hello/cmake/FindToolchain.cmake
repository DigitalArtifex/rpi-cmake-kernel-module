# Determine if we are cross-compiling.
# If we are, the rootfs from the raspberry pi
# will need to be set-up per the instructions in
# the read-me
if(TOOLCHAIN_CROSS_COMPILE)
    if(WIN32)
        message(FATAL "Cannot cross compile from windows. Please run a linux distribution in a virtual manager")
    endif()

    set(CMAKE_SYSTEM_NAME Linux)
    set(CMAKE_SYSTEM_PROCESSOR arm)

    if(TOOLCHAIN_BOARD_VERSION STREQUAL "NONE")
        message(FATAL "Target board has not been set.")
    elseif(TOOLCHAIN_BOARD_VERSION "^(0|0w|1|1a|1b|1b+|compute)")
        # Check for GCC
        if(NOT EXISTS ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabi-gcc)
            message(FATAL "arm-linux-gnueabi-gcc package not found in ${TOOLCHAIN_ROOT}/bin. Please specify an appropriate toolchain directory")
        # Check for G++
        elseif(NOT EXISTS ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabi-g++)
            message(FATAL "arm-linux-gnueabi-g++ package not found in ${TOOLCHAIN_ROOT}/bin. Please specify an appropriate toolchain directory")
        else()
            set(CMAKE_C_COMPILER ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabi-gcc)
            set(CMAKE_CXX_COMPILER ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabi-g++)
        endif()
    elseif(TOOLCHAIN_BOARD_VERSION MATCHES "^(2|2a|2b|2b+|3|3a|3b|3b+|4)")
        # Check for GCC
        if(NOT EXISTS ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabihf-gcc)
            message(FATAL "arm-linux-gnueabihf-gcc package not found in ${TOOLCHAIN_ROOT}/bin. Please specify an appropriate toolchain directory")
        # Check for G++
        elseif(NOT EXISTS ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabihf-g++)
            message(FATAL "arm-linux-gnueabihf-g++ package not found in ${TOOLCHAIN_ROOT}/bin. Please specify an appropriate toolchain directory")
        else()
            set(CMAKE_C_COMPILER ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabihf-gcc)
            set(CMAKE_CXX_COMPILER ${TOOLCHAIN_ROOT}/bin/arm-linux-gnueabihf-g++)
        endif()
    endif()

    set(CMAKE_SYSROOT ${TOOLCHAIN_ROOTFS})
    set(CMAKE_STAGING_PREFIX /home/devel/stage)

    set(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
    set(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)
    set(CMAKE_FIND_ROOT_PATH_MODE_PACKAGE ONLY)
endif()
